#include <stdio.h>
struct student {
    char FirstName[30];
    char SubjectName[20];
    int RefNum;
    float marks;
} s[10];

int main() {
    int i;
    printf("ENTER INFORMATION OF STUDENTS:\n");

    // storing information
    for (i = 0; i < 5; ++i) {
        s[i].RefNum = i + 1;
        printf("\nReference Number%d,\n", s[i].RefNum);
        printf("ENTER FIRST NAME: ");
        scanf("%s", s[i].FirstName);
        printf("ENTER THE SUBJECT NAME: ");
        scanf("%s", s[i].SubjectName);
        printf("ENTER THE MARKS OF THE PARICULAR SUBJECT: ");
        scanf("%f", &s[i].marks);
    }
    printf("\n\n\nDISPLAYING INFORMATION:\n\n");

    // displaying information
    for (i = 0; i < 5; ++i) {
        printf("\nReference Number: %d\n", i + 1);
        printf("FIRST NAME: ");
        puts(s[i].FirstName);
        printf("SUBJECT: ");
        puts(s[i].SubjectName);
        printf("MARKS OF THE PARTICULAR SUBJECT: %.1f", s[i].marks);
        printf("\n");
    }
    return 0;
}
